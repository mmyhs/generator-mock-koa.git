### generator-mock-koa
使用koa结合mock，主要提供给前端开发人员的模拟数据测试，必要的时候，也可以用于代理转发、和基于Koa开发新的系统


## Installation

First, install [Yeoman](http://yeoman.io) and generator-mock-koa using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-mock-koa
```

Then generate your new project:

```bash
yo mock-koa
```
#### 1.1.2
> 实现了基础的功能，能够基于简单配置实现数据的mock
> 升级特性：构建完成后，会自动安装相关依赖，使用者只需要到根目录下执行 `npm run mock-serve` 就能开启mock服务了

## Use

> 项目构建完成后，打开`src/api`，在这个页面下编写mock的接口，在该目录下的文件会自动处理，要求符合格式

**案列：使用模板解析语法开发**

```js
import Mock from 'mockjs';

const data = Mock.mock({
  total: 100,
  'rows|10': [{
    id: '@guid',
    name: '@cname',
    'age|20-30': 23,
    'job|1': ['前端工程师', '后端工程师', 'UI工程师', '需求工程师']
  }]
});

export default {
  prefix: "/pattern",
  methods: {
    "GET /a": data,
    "GET /list": () => {
      return {
        code: 200,
        message: "success",
        data
      }
    },
    // 这里的 ctx ==> router.get(ctx);
    "/list2": (ctx) => {
      return {
        code: 200,
        message: "success",
        data: ctx.request.query
      }
    },
    "POST /form": () => {
      return {
        code: 200,
        message: "success",
        data: Mock.mock({ id: '@guid' })
      }
    }
  }
};

```



**案列，使用原生Koa的开发模式**

```js
import Router from 'koa-router';
import Mock from 'mockjs';

/**
 * 案列，使用原生Koa的开发模式
 */

const router = new Router({
  prefix: '/user',
});

/**
 * 获取菜单列表
 */
router.get('/list', (ctx) => {
  const data = Mock.mock({
    total: 100,
    'rows|10': [{
      id: '@guid',
      name: '@cname',
      'age|20-30': 23,
      'job|1': ['前端工程师', '后端工程师', 'UI工程师', '需求工程师']
    }]
  });
  return {
    code: 200,
    message: "success",
    data
  }
});

export default router;

```



## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).


## License

MIT © [mmyhs]()


[npm-image]: https://badge.fury.io/js/generator-mock-koa.svg
[npm-url]: https://npmjs.org/package/generator-mock-koa
[travis-image]: https://travis-ci.com//generator-mock-koa.svg?branch=master
[travis-url]: https://travis-ci.com//generator-mock-koa
[daviddm-image]: https://david-dm.org//generator-mock-koa.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-mock-koa
