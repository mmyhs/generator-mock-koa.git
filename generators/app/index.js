// 作为 generator 的 核心入口
// 需要导入一个继承自 Yeoman Generator 的一些生命周期方法
// Yeoman Generator 在工作的时候回自动调用 再次类型中定义的 一些生命周期方法

// https://blog.csdn.net/weixin_34269583/article/details/92476337
// https://www.jianshu.com/p/038c6b91f667

const Generator = require('yeoman-generator');
const path = require('path');


module.exports = class extends Generator {

  promiting () {
    return this.prompt([
      {
        type: 'input', // 用户输入的方式
        name: 'projectName',
        message: 'your project name', // 提示
        default: this.appname, // appname 为项目生成目录的名称
      }, {
        type: 'input', // 用户输入的方式
        name: 'rootBaseUrl',
        message: 'your project context path', // 提示
        default: "/" + this.appname, // appname 为项目生成目录的的上下文
      }, {
        type: 'input', // 用户输入的方式
        name: 'port',
        message: 'your project prot', // 提示
        default: 9090, // appname 为项目启动的端口
      }, {
        type: 'list',
        name: 'installType',
        message: 'select an installation mode:',
        choices: [
          {
            name: 'npm',
            value: 'npm',
            checked: true   // 默认选中
          }, {
            name: 'yarn',
            value: 'yarn'
          }
        ]
      },
    ]).then(answers => {
      this.answers = answers
    })
  }

  install () {
    var done = this.async();

    const { projectName, installType } = this.answers;

    this.spawnCommand(installType, ['install'])  //安装项目依赖
      .on('exit', (code) => {
        if (code) {
          done(new Error('code:' + code));
        } else {
          this.log(`cd ${projectName} && ${installType} run mock-serve`);
          done();
        }
      })
      .on('error', done);
  }

  // Yeoman 自动在生成文件阶段调用这个方法
  writing () {
    // 通过模板方法 协议文件到目标目录
    // 这里可以定义一个 数组，然后 通过遍历的方式，一次性生成一大堆文件

    const { projectName, rootBaseUrl, port } = this.answers;

    this.destinationRoot(projectName);
    [
      { dir: 'src', isFile: true },
      { dir: '.babelrc', isFile: true },
      { dir: 'nodemon.json', isFile: true },
      // { dir: 'package.json', isFile: true },
    ].forEach(item => {
      const tmpl = this.templatePath(item.dir);
      let output = null;
      if (item.isFile) {
        output = this.destinationPath(path.resolve(projectName, item.dir));
      } else {
        output = this.destinationPath(projectName);
      }

      this.fs.copyTpl(tmpl, output, {})
    });

    const emitEjsTmpl = (pathStr, ejsData) => {
      const tmpl = this.templatePath(pathStr);
      const output = this.destinationPath(path.resolve(projectName, ...pathStr.split('_')));
      this.fs.copyTpl(tmpl, output, ejsData)
    }

    emitEjsTmpl("package.json", { name: projectName });
    emitEjsTmpl("src_config/core.js", { rootBaseUrl, port });

  }
}
