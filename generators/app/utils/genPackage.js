const genPackage = (dependencies, mode = "") => {
  const result = []
  Object.keys(dependencies).map(key => {
    result.push(`${key}@${dependencies[key]}`)
  })
  if (mode) {
    result.push(mode)
  }
  return result;
}

module.exports = {
  genPackage
}
