/**
 * 统计请求时间
 * @param {*} ctx
 * @param {*} next
 */

const responseTime = async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
}

module.exports = {
  responseTime
}
