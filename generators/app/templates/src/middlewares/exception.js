/**
 * 处理接口返回的信息
 */
const catchError = async (ctx, next) => {
  try {
    const result = await next();
    if (result) {
      ctx.body = result;
    }
  } catch (e) {
    // 未知异常
    const { path, method } = ctx.request;
    ctx.body = new Exception('服务器开小差了~', '999', `${method} ${path}`);
    ctx.status = 500;
  }
}

module.exports = {
  catchError
}
