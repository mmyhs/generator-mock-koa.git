import Mock from 'mockjs';

const data = Mock.mock({
  total: 100,
  'rows|10': [{
    id: '@guid',
    name: '@cname',
    'age|20-30': 23,
    'job|1': ['前端工程师', '后端工程师', 'UI工程师', '需求工程师']
  }]
});

export default {
  prefix: "/pattern",
  methods: {
    "GET /a": data,
    "GET /list": () => {
      return {
        code: 200,
        message: "success",
        data
      }
    },
    "/list2": (ctx) => {
      return {
        code: 200,
        message: "success",
        data: ctx.request.query
      }
    },
    "POST /form": () => {
      return {
        code: 200,
        message: "success",
        data: Mock.mock({ id: '@guid' })
      }
    }
  }
};
