import Router from 'koa-router';
import Mock from 'mockjs';

/**
 * 案列，使用原生Koa的开发模式
 */

const router = new Router({
  prefix: '/user',
});

/**
 * 获取菜单列表
 */
router.get('/list', (ctx) => {
  const data = Mock.mock({
    total: 100,
    'rows|10': [{
      id: '@guid',
      name: '@cname',
      'age|20-30': 23,
      'job|1': ['前端工程师', '后端工程师', 'UI工程师', '需求工程师']
    }]
  });
  return {
    code: 200,
    message: "success",
    data
  }
});

export default router;
