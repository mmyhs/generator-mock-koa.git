const Koa = require('koa');
const { getIPAddress } = require('./utils/os');
const initManager = require('./core/initManager');
import config from './config/core'

const addressList = getIPAddress();

const PORT = config.PORT;

const app = new Koa();

initManager(app);

app.listen(PORT, () => {
  console.log('Mock Data 启动');
  console.log(`- Local:  \x1B[36mhttp://localhost:${PORT}/\x1B[0m`);
  addressList.forEach((address) => {
    // #22A3C7
    console.log(`- Network: \x1B[36mhttp://${address}:${PORT}/\x1B[0m`);
  });
});
