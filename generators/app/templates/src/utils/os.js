const os = require('os');

/**
 * 获取本机的IP
 * @returns
 */
const getIPAddress = () => {
  const address = [];
  const interfaces = os.networkInterfaces();
  Object.keys(interfaces).forEach((devName) => {
    const iface = interfaces[devName];
    for (let i = 0; i < iface.length; i += 1) {
      const alias = iface[i];
      if (alias.family === 'IPv4' && !alias.internal) {
        address.push(alias.address);
      }
    }
  });
  return address;
};

module.exports = {
  getIPAddress
}
