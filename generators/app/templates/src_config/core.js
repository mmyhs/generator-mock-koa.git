/**
 * 核心的配置文件专用
 */
export default {
  /** 接口地址访问上下文 */
  ROOT_BASE_URL: "<%=rootBaseUrl %>",
  /** 端口 */
  PORT: "<%=port %>",
}
